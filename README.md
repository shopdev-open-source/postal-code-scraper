# postal code scraper

Project to scrape the data from various sources and build a local database for faster and centralizes access to information regarding Brazilian Postal Code Database.

## Data Sources


##### 1.  https://www.ibge.gov.br/explica/codigos-dos-municipios.php
  
*Retrieve the complete states and cities list with codes*

##### 2.  http://www.correios.com.br/precisa-de-ajuda/busca-cep/alteracao-de-faixas-de-cep/pdf/DNE_DatadeCodificaodeCidades.pdf

*Get information on what cities have multiple postal codes, one for each street*

##### 3.  http://www.buscacep.correios.com.br/sistemas/buscacep/buscaFaixaCep.cfm

*Scrape the used and reserved postal code ranges for each city*

##### 4.  Correios WebServices - accesible thru https://github.com/mstuttgart/pycep-correios

*Scrape the curretly valid postal code information for each valid postal code*

## Goals

The main goal is to have a dataset containing all updated data from Postal Code database locally and be able to access it in a faster and more reliable way.